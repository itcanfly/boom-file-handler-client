import requests


class FileHandlerClient(object):

    def __init__(self, base_url="https://file-handler:5453/v1"):
        if base_url.endswith("/"):
            base_url = base_url[:-1]
        self.base_url = base_url

    def _get_file_by_uuid(self, uuid):
        return requests.get(self.base_url+"/v1/get/?uuid="+uuid).json()

    def get_fd_by_uuid(self, uuid, mode="rb"):
        data = self._get_file_by_uuid(uuid)
        path = data.get("path")
        return open(path, mode)

    def get_url_by_uuid(self, uuid):
        data = self._get_file_by_uuid(uuid)
        return data.get("url")

    def upload_file(self, path):
        with open(path, "rb") as f:
            return requests.post(self.base_url+"/v1/upload/", files={"file": f}).json()

    def upload_content(self, f):
        return requests.post(self.base_url+"/v1/upload/", files={"file": f}).json()

    def upload_file_from_url(self, url):
        response = requests.get(url, stream=True)
        if response.status_code == 200:
            response.raw.decode_content = True
            *_, file_name = url.split('/')
            file_name, *_ = file_name.split('?')
            response.raw.name = file_name
            return self.upload_content(response.raw)
