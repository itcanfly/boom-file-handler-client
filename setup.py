#!/usr/bin/env python

from setuptools import setup

requires = [
    "requests",
]

setup(name='boom-file-handler-client',
      version='0.0.1',
      description='Highlevel API for Boompublic file handler',
      author='Vladimir A Filonov',
      author_email='vladimir@itcanfly.org',
      packages=['boom_fhc'],
      install_requires=requires
)
